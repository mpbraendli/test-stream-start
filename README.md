# test-stream-start

Test `stream0_start_clks` behaviour.

## Build

Prerequisites: libiio including dev package, gcc compiler

Build:

    make

## Issues

If you get

    ==3963==ASan runtime does not come first in initial library list; you should either link runtime to your application or manually preload it with LD_PRELOAD.

it's maybe because raspian preloads some alternative memcpy implementation.
Comment or remove the line mentioning "libarmmem" from /etc/ld.so.preload.
