#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "iio.h"
const uint64_t IIO_TIMEOUT_MS = 1000;
const size_t TRANSMISSION_FRAME_LEN_SAMPS = (2656 + 76 * 2552) * /* I+Q */ 2;
const size_t IIO_BUFFERS = 1;
const size_t IIO_BUFFER_LEN_SAMPS = TRANSMISSION_FRAME_LEN_SAMPS / IIO_BUFFERS;

static char err_msg[256];

static void *m_ctx = NULL;
static struct iio_device *m_dexter_dsp_tx = NULL;
static struct iio_device *m_ad9957_tx0 = NULL;
static struct iio_channel *m_tx_channel = NULL;
static struct iio_buffer *m_buffer = NULL;


static void write_start_clks(long long value)
{
    int r = iio_device_attr_write_longlong(m_dexter_dsp_tx, "stream0_start_clks", value);
    if (r != 0) {
        iio_strerror(-r, err_msg, sizeof(err_msg));
        fprintf(stderr, "IIO Attr write stream0_start_clks: %s\n", err_msg);
        exit(1);
    }
}

static void channel_up()
{
    fprintf(stderr, "DEXTER CHANNEL_UP\n");

    const int CHANNEL_INDEX = 0;
    m_tx_channel = iio_device_get_channel(m_ad9957_tx0, CHANNEL_INDEX);
    assert(m_tx_channel);

    iio_channel_enable(m_tx_channel);

    m_buffer = iio_device_create_buffer(m_ad9957_tx0, IIO_BUFFER_LEN_SAMPS, 0);
    assert(m_buffer);
}

static void channel_down()
{
    iio_channel_disable(m_tx_channel);

    fprintf(stderr, "DEXTER CHANNEL_DOWN\n");
    if (m_buffer) {
        iio_buffer_destroy(m_buffer);
        m_buffer = NULL;
    }
}


int main(int argc, char **argv)
{
    m_ctx = iio_create_local_context();
    assert(m_ctx);

    int r = iio_context_set_timeout(m_ctx, IIO_TIMEOUT_MS);
    assert(r == 0);

    m_dexter_dsp_tx = iio_context_find_device(m_ctx, "dexter_dsp_tx");
    assert(m_dexter_dsp_tx);

    m_ad9957_tx0 = iio_context_find_device(m_ctx, "ad9957_tx0");
    assert(m_ad9957_tx0);

    long long pps_clks = 0;
    r = iio_device_attr_read_longlong(m_dexter_dsp_tx, "pps_clks", &pps_clks);
    if (r != 0) {
        iio_strerror(-r, err_msg, sizeof(err_msg));
        fprintf(stderr, "IIO Attr read pps_clks: %s\n", err_msg);
        exit(1);
    }

    write_start_clks(0);


    const size_t DSP_CLOCK = 80 * 2048000uLL;
    const size_t OFFSET_S = 2;

    write_start_clks(pps_clks + OFFSET_S * DSP_CLOCK);

    channel_up();

    const size_t buflen_samps = TRANSMISSION_FRAME_LEN_SAMPS / IIO_BUFFERS;
    const size_t buflen = buflen_samps * sizeof(int16_t);

    memset(iio_buffer_start(m_buffer), 0, buflen);
    ssize_t pushed = iio_buffer_push(m_buffer);
    if (pushed < 0) {
        iio_strerror(-r, err_msg, sizeof(err_msg));
        fprintf(stderr, "iio_buffer_push: %s\n", err_msg);
    }
    fprintf(stderr, "Pushed %zd\n", pushed);

    channel_down();

    iio_context_destroy(m_ctx);

}
